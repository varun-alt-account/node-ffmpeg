var config = {};

var path = require("path");

config.bins = {
	ffmpeg: path.join(__dirname, "bin", process.platform, "ffmpeg"),
	ffprobe: path.join(__dirname, "bin", process.platform, "ffprobe")
};

config.progressKeys = ['frame', 'fps', 'q', 'size', 'time', 'bitrate', 'dup', 'drop']; // possible keys passed back from progress

module.exports = config;